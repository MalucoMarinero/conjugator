require 'open-uri'
require 'iconv'
require 'rubygems'
require 'hpricot'
require 'jcode'

$KCODE = 'u'

# RemoveAccents version 1.0.3 (c) 2008-2009 Solutions Informatiques Techniconseils inc.
# 
# This module adds 2 methods to the string class. 
# Up-to-date version and documentation available at:
#
# http://www.techniconseils.ca/en/scripts-remove-accents-ruby.php
#
# This script is available under the following license :
# Creative Commons Attribution-Share Alike 2.5.
#
# See full license and details at :
# http://creativecommons.org/licenses/by-sa/2.5/ca/
#
# Version history:
#   * 1.0.3 : July 23 2009
#               Corrected some incorrect character codes. Source is now wikipedia at:
#                 http://en.wikipedia.org/wiki/ISO/IEC_8859-1#Related_character_maps
#               Thanks to Raimon Fernandez for pointing out the incorrect codes.
#   * 1.0.2 : October 29 2008
#               Slightly optimized version of urlize - Jonathan Grenier (jgrenier@techniconseils.ca)
#   * 1.0.1 : October 29 2008
#               First public revision - Jonathan Grenier (jgrenier@techniconseils.ca)
#

class String
  # The extended characters map used by removeaccents. The accented characters 
  # are coded here using their numerical equivalent to sidestep encoding issues.
  # These correspond to ISO-8859-1 encoding.
  ACCENTS_MAPPING = {
    'E' => [200,201,202,203],
    'e' => [232,233,234,235],
    'A' => [192,193,194,195,196,197],
    'a' => [224,225,226,227,228,229,230],
    'C' => [199],
    'c' => [231],
    'O' => [210,211,212,213,214,216],
    'o' => [242,243,244,245,246,248],
    'I' => [204,205,206,207],
    'i' => [236,237,238,239],
    'U' => [217,218,219,220],
    'u' => [249,250,251,252],
    'N' => [209],
    'n' => [241],
    'Y' => [221],
    'y' => [253,255],
    'AE' => [306],
    'ae' => [346],
    'OE' => [188],
    'oe' => [189]
  }
  
  
  # Remove the accents from the string. Uses String::ACCENTS_MAPPING as the source map.
  def removeaccents    
    str = String.new(self)
    String::ACCENTS_MAPPING.each {|letter,accents|
      packed = accents.pack('U*')
      rxp = Regexp.new("[#{packed}]", nil)
      str.gsub!(rxp, letter)
    }
    
    str
  end
  
  
  # Convert a string to a format suitable for a URL without ever using escaped characters.
  # It calls strip, removeaccents, downcase (optional) then removes the spaces (optional)
  # and finally removes any characters matching the default regexp (/[^-_A-Za-z0-9]/).
  #
  # Options
  #
  # * :downcase => call downcase on the string (defaults to true)
  # * :convert_spaces => Convert space to underscore (defaults to false)
  # * :regexp => The regexp matching characters that will be converting to an empty string (defaults to /[^-_A-Za-z0-9]/)
  def urlize(options = {})
    options[:downcase] ||= true
    options[:convert_spaces] ||= false
    options[:regexp] ||= /[^-_A-Za-z0-9]/
    
    str = self.strip.removeaccents
    str.downcase! if options[:downcase]
    str.gsub!(/\ /,'_') if options[:convert_spaces]
    str.gsub(options[:regexp], '')
  end
end

class ConjugatorParser
    def initialize
        @website_url = 'http://leconjugueur.lefigaro.fr/conjugaison/verbe'
        @encoding = 'iso-8859-15'
        @temps = []
        @pages = []
    end

    def get_verbes(verbes)
        @verbes = verbes.split(' ')
        @verbes.each do |verb|
            path = verb.urlize
            raw_html = open("#{@website_url}/#{path}.html").gets
            ic = Iconv.new('UTF-8//IGNORE', @encoding)
            valid_html = ic.iconv(raw_html)
            @pages << Hpricot(valid_html)
        end
    end

    def get_temps(temps)
        @verbes.each_with_index do |verb, index|
        temps.each do |temp|
            get = 1
            words = temp.split(' ')
            imperative = words.shift
            if imperative == "Indicatif" then get = 1 end
            if imperative == "Subjonctif" then get = 2 end
            if imperative == "Conditionnel" then get = 3 end
            if imperative == "Impératif" then get = 4 end
            depth = 1
            if words.join(' ') == "Passé première forme" then depth = 3 end
            if words.join(' ') == "Passé deuxième forme" then depth = 3 end
            if words.join(' ') == "Passé"
                if get == 2 then depth = 2 end
                if get == 4 then depth = 3 end
            end
            (@pages[index]/"td").each do |td|
                if (td/"u").inner_html == words.join(' ')
                    title = "%s %s" % [imperative, words.join(' ')]
                    if depth == get then @temps.push(
                                         {'title' => title, 'td' => td,
                                          'verb' => verb,
                                         'imperative' => imperative}) end
                    depth += 1
                end
            end
        end
        end
    end

    def output
        @cards = []
        @temps.each do |temp|
            lines = temp['td'].inner_html.split('<br />')
            heading = lines.shift.sub(%r{<u>([^<]+)</u>},
                                      '\k\1>')
            heading = temp['title']
            lines.each_with_index do |line, index|
                if line != "-" then
                if temp['imperative'] == "Impératif"
                    if index == 0 then blanked = '(tu) _________ ' end
                    if index == 1 then blanked = '(nous) _________ ' end
                    if index == 2 then blanked = '(vous) _________ ' end
                else
                    blanked = line.sub(%r{(j'|je|tu|ils|il|nous|vous).*}, '\1 _________')
                end
                # blanked = line.gsub(%r{<b>([^<]*)</b>}, '_____')
                answer = line.gsub(%r{</?b>}, '')
                @cards.push({
                    'verbes' => temp['verb'],    'temp' => heading,
                    'blanked' => blanked,   'answer' => answer,
                    })
                end
            end
        end
        output = []
        @cards.each do |card|
            output.push "%s (%s)<br />%s\t%s" % 
             [card['verbes'], card['temp'], card['blanked'], card['answer']]
        end
        puts output
        return output
    end
end


class ConjugatorCLI
    def initialize
        puts "----------------------------------------\n"
        puts "Starting the Conjugator\n"
        puts "----------------------------------------\n"
        puts "\n"
        @parser = ConjugatorParser.new
        @temps_options = [
            "Indicatif Présent",
            "Indicatif Passé composé",
            "Indicatif Imparfait",
            "Indicatif Plus-que-parfait",
            "Indicatif Passé simple",
            "Indicatif Passé antérieur",
            "Indicatif Futur simple",
            "Indicatif Futur antérieur",
            "Subjonctif Présent",
            "Subjonctif Passé",
            "Subjonctif Imparfait",
            "Subjonctif Plus-que-parfait",
            "Conditionnel Présent",
            "Conditionnel Passé première forme",
            "Conditionnel Passé deuxième forme",
            "Impératif Présent",
            "Impératif Passé",
        ]
    end

    def get_verbes(verbes='')
        if verbes != true
            puts "Choisissez les verbes: "
            @verbes = gets.strip
            puts "\n"
        end
    end

    def get_temps(temps='')
        if temps != true
            output = "Choisissez les temps (séparés par un espace): \n"
            @temps_options.each_with_index do |temp, count|
                  output += "%d) %s\t" % [count, temp]
            end
            puts output
            @temps_indexes = gets.strip
            puts "\n"
            @temps_indexes = @temps_indexes.split ' '
            @query_temps = []
            @temps_indexes.each do |index|
                @query_temps << @temps_options[index.to_i]
            end
        end
    end

    def output_file(filename='')
        verb_filename = @verbes.split(' ').join('-').urlize
        if filename == ''
            filename = "%s-%s.txt" % [verb_filename, @temps_indexes.join('-')]
        end
        puts "Outputting to %s" % filename
        File.open(filename, "w") do |file|
            @parser.get_verbes @verbes
            @parser.get_temps @query_temps
            file.puts @parser.output
        end
        puts "Voilà! Votre fichier a été écrit à %s" % filename
    end

end

cli = ConjugatorCLI.new
cli.get_verbes
cli.get_temps
cli.output_file


=begin
File.open("pouvoir-1-2.txt", "w") do |file|
    parser = ConjugatorParser.new
    parser.get_verbes "aller"
    parser.get_temps ["Indicatif Présent","Indicatif Futur simple",
                      "Conditionnel Passé deuxième forme"]
    file.puts parser.output
end
=end



