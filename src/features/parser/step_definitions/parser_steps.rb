begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end
require 'rubygems'
require 'cucumber/formatter/unicode'
require_relative '../../../conjugator'

Before do
    @parser = ConjugatorParser.new
end

After do
end

Given /I select the verbes (\w+)/ do |verb|
    @parser.get_verbes verb
end

Given /I select the temps (.*)/ do |temps|
    temps = temps.split(/\s*,\s*/)
    @parser.get_temps temps
end

Then /I should have an output with (\d+) lines/ do |lines|
    lines = lines.to_i
    @result = @parser.output
    #@result.each {|line| puts line}
    @result.length.should == lines
end
