Feature: Search for French Conjugations and output to screen or a text file.
         Should be a command line application, packaged into exe for distro.
         Should be simple to use. Tested with BDD

Scenario: Example Usage and Output
    Given I select the verbes pouvoir
        And I select the temps Présent, Futur simple
    Then I should have an output with 30 lines
