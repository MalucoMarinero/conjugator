import Data.Char
import Data.List
import Data.Maybe
import Data.List.Split
import Network.HTTP
import Text.HTML.TagSoup
import Text.RegexPR
import Text.Printf
import Debug.Trace

data Card = Card { verbe :: String
                 , temp :: String
                 , blanked :: String
                 , answer :: String
                 } deriving (Show)

data PageData = PageData { pverbe :: String
                         , ptemps :: [(String, [String])]
                         } deriving (Show)

allTemps = ["Indicatif Pr\233sent",
            "Indicatif Pass\233 compos\233",
            "Indicatif Imparfait",
            "Indicatif Plus-que-parfait",
            "Indicatif Pass\233 simple",
            "Indicatif Pass\233 ant\233rieur",
            "Indicatif Futur simple",
            "Indicatif Futur ant\233rieur",
            "Subjonctif Pr\233sent",
            "Subjonctif Pass\233",
            "Subjonctif Imparfait",
            "Subjonctif Plus-que-parfait",
            "Conditionnel Pr\233sent",
            "Conditionnel Pass\233 premi\232re forme",
            "Conditionnel Pass\233 deuxi\232me forme",
            "Imp\233ratif Pr\233sent",
            "Imp\233ratif Pass\233"]

openURL x = getResponseBody =<< simpleHTTP (getRequest x)

main = do
    putStrLn $ "Welcome to the Conjugator"
    putStrLn $ "Choisissez les verbes (s\233par\233s par un espace): "
    verbsString <- getLine
    putStrLn $ "\n"
    putStrLn $ makeTempsPrompt
    putStrLn $ "Choisissez les temps (s\233par\233s par un espace):"
    tempsString <- getLine
    let verbs = splitOn " " verbsString
    let tempIndexes = splitOn " " tempsString
    let temps = tempIndexesToTemps tempIndexes
    pages <- getAllPages verbs 
    let pagedatas = getAllPageData pages temps
    let cards = allPageDataToCards pagedatas
    let filename = makeFileName verbs tempIndexes
    putStrLn $ "Outputting to " ++ filename
    writeFile filename (renderCards cards)
    putStrLn $ "Voil\224! Votre fichier a \233t\233 \233crit \224 " ++ filename


makeFileName vs tis =
    let vvs = intercalate "-" vs
        ttis = intercalate "-" tis
    in  vvs ++ "-" ++ ttis ++ ".txt"

tempIndexesToTemps :: [String] -> [String]
tempIndexesToTemps ts =
    let is = [read t :: Int | t <- ts]
    in  [allTemps !! i | i <- is]

makeTempsPrompt = concat [last (words (show (elemIndex t allTemps))) ++ ") " ++ t ++ "\t " | t <- allTemps]

getAllPages vs = mapM getVerbePage vs 
getVerbePage x = fmap parseTags $ openURL $ "http://leconjugueur.lefigaro.fr/conjugaison/verbe/" ++ x ++ ".html"

getAllPageData ps ts = [parseVerbePage p ts | p <- ps]

renderCards :: [Card] -> String
renderCards cards = intercalate "\n" [renderCard c | c <- cards ]

renderCard :: Card -> String
renderCard card = printf "%s (%s)<br />%s\t%s" (verbe card) (temp card)
                                               (blanked card) (answer card)

allPageDataToCards :: [PageData] -> [Card]
allPageDataToCards ps =
    let cs = [pageDataToCards p | p <- ps]
    in  concat cs

pageDataToCards :: PageData -> [Card]
pageDataToCards pagedata =
    let verb = pverbe pagedata
        temps = ptemps pagedata
        cardLists = [ createCardList verb t as | (t, as)<- temps]
    in  concat cardLists

createCardList :: String -> String -> [String] -> [Card]
createCardList verb temp answers 
    | answers == [] = []
    | ltemp == "Imp\233ratif" = 
                    [Card verb temp b a | a <- answers, b <- impBln,
                           b `elemIndex` impBln == a `elemIndex` answers]
    | otherwise = [Card verb temp (blankAnswer a) a | a <- answers]  
    where ltemp = head $ words temp
          impBln = ["(tu) ________", "(nous) _________", "(vous) _________"] 

blankAnswer a = subRegexPR "(j'|je|tu|ils|il|nous|vous).*" "\\1 _________" a

-- | Turn a verb page into page data
parseVerbePage :: [Tag String] -> [String] -> PageData
parseVerbePage page templist = PageData {pverbe=header, ptemps=theTemps}
    where   theTemps = [(t, (tempTagsToStrings $ getTempTags t page)) | t <- templist]
            header = getConjugaisonHeader page


getConjugaisonHeader page = 
    let TagText s = head $drop 1 $ head $ sections (~== "<h1>") page
        ss = words s
        header = last ss
    in  [toLower c | c <- header]
        
tempTagsToStrings :: [Tag String] -> [String]
tempTagsToStrings tags = tagsToJoinedStrings splitted
    where   splitted = splitAtBreaks cullBold
            cullBold = drop 1 [t | t <- tags, t ~/= "<br>", t ~/= "<b>",
                                              t ~/= "</b>", t ~/= "</u>"]

-- | Converts List of TagText Lists into List of Strings
tagsToJoinedStrings :: [[Tag String]] -> [String]
tagsToJoinedStrings ts = [joinTextTags t | t <- ts, joinTextTags t /= "-"]

-- | Concatenates TagText List to a String
joinTextTags :: [Tag String] -> String
joinTextTags ts = concat [fromTagText t | t <- ts]

-- | Splits up the tag list into lists of answers
splitAtBreaks :: [Tag String] -> [[Tag String]]
splitAtBreaks [] = []
splitAtBreaks [TagClose "tr", TagOpen "tr" []] = []
splitAtBreaks (TagClose "tr":TagOpen "tr" []:xs) = []
splitAtBreaks tags = [newSplit] ++ (splitAtBreaks newTags)
    where   newSplit = (fst vals)
            newTags = drop 1 $ snd vals
            vals = splitAt (i) tags
            Just i = head is
            is = [elemIndex t tags | t <- tags, 
                        t == (TagClose "br") || t == (TagClose "td")]

-- | Isolates TagTexts required for answers.
getTempTags :: String -> [Tag String] -> [Tag String]
getTempTags temp tags = tempTags
    where   tempTags = drop 1 $takeWhile (/= TagOpen "td" []) longTempTags
            longTempTags = dropWhile (/= TagText area) sect
            sect = head $ partitions (~== head ss) tags
            area = intercalate " " (tail ss)
            ss = words temp

